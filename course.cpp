
#include <string>
#include <iostream>
#include "course.h"
#include <vector>


#define G_AMOUNT 3

/*
The function initates a course
input: 2 grades, of a test 1 grade of an exam and a name
output: None
*/
void Course::init(std::string name, unsigned int test1, unsigned int test2, unsigned int exam)
{
	this->_name = name;
	this->_test1 = test1;
	this->_test2 = test2;
	this->_exam = exam;
}

/*
The function gets the name of the course
Input: none
output: The name of the course
*/
std::string Course::getName()
{
	return(this->_name);
}

/*
The function calculates the final grade
Input: None 
Output : The final grade
*/
double Course::getFinalGrade()
{
	return(double(this->_test1/4)+double(this->_test2/4)+double(this->_exam/2));
}


/*
The function gives u an array of the grades
Input: NOne 
Output: The grades
*/
std::vector<int> Course::getGradesList()
{
	std::vector<int> grades;
	grades.push_back(this->_test1);
	grades.push_back(this->_test2);
	grades.push_back(this->_exam);
	return grades;
}