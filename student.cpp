#include "student.h"
#include <string>
#include <iostream>

/*
The fucntion initates a student
Input: the students name, coursrs , and the amount of courses
output: None
*/
void Student::init(std::string name, Course** courses, unsigned int crsCount)
{
	this->_name = name;
	this->_courses = new Course*[sizeof(Course*)];
	this->_crsCount = crsCount;
}

/*
The function returns the name of the student
Input: None 
Output: The name of the student
*/
std::string Student::getName()
{
	return(this->_name);
}

/*
The function changes the name of the student
Input: The new name
Output: None
*/
void Student::setName(std::string name)
{
	this->_name = name;
}

/*
The function returns the amount of the courses the student has
Input: NOne
Output: The course amount
*/
unsigned int Student::getCrsCount()
{
	return(this->_crsCount);
}


/*
The function returns the list of the courses
Input : None 
Output: The courses
*/
Course** Student::getCourses()
{
	return(this->_courses);
}

/*

*/
double Student::getAvg()
{
	double sum = 0;
	for (int i = 0; i < this->_crsCount; i++)
	{
		sum += _courses[i]->getFinalGrade();
	}
	sum = sum / _crsCount;
	return sum;
}