#pragma once
#include <string>
#include <vector>

/* Representation of a Course grade-book
test - 25%
final exam - 50%
can only change grades after creation.
*/

class Course
{
public:
	void init(std::string name, unsigned int test1, unsigned int test2, unsigned int exam);
	std::vector<int> getGradesList();
	std::string getName();
	double getFinalGrade();

private:
	std::string _name;
	unsigned int _test1;
	unsigned int _test2;
	unsigned int _exam;

};
