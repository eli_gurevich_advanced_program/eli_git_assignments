#include <string>
#include "string.h"

/*
The function initiates a string
input: a string
output: None
*/
void myString::init(std::string str)
{
	this->_str = str;
}
/*
The function returns the string
Input: None 
Output : The string 
*/
std::string myString::getStr()
{
	return(_str);
}
/*
Input: none
Output: The length of the string
The function calculates the length of the string
*/
unsigned int myString::getLength()
{
	return(this->_str.length());
}
/*
input: Char
Output: the value in the string
the function gets an index and returns the value in that index
*/
char myString::getCharUsingSquareBrackets(unsigned int index)
{
	return(this->_str[index]);
}
/*
Inout: Index
Output: A value at the index
the function gets a value from an index 
*/
char myString::getCharUsingRoundBrackets(unsigned int index)
{
	return (this->_str.at(index));
}
/*
The function returns the first char in a string
Input: None
Output:The first char
*/
char myString::getFirstChar()
{
	return(this->_str[0]);
}
/*
The function returns the last char in a string
Input: None
Output:The last char
*/
char myString::getLastChar()
{
	return(this->_str[_str.length() - 1]);
}
/*
Input: a char
Output: The index of the char
The function searches for the first index of a char
*/
int myString::getCharFirstOccurrenceInd(char c)
{
	return(this->_str.find_first_of(c));
}
/*
input: a char
Output: The index
The function searches for the index of the last char 
*/
int myString::getCharLastOccurrenceInd(char c)
{
	return(this->_str.find_last_of(c));
}
/*
Input: a string
Output: the position of the first char that is the same
The function searches for the first occurance of the string sent and the string in the class
*/
int myString::gstStringFirstOccurrenceInd(std::string str)
{
	return(this->_str.find_first_of(str));
}
/*
The function checks if the strings are equal
Input: a string
Output: true / false
*/
bool myString::isEqual(std::string s)
{
	return(this->_str.compare(s));
}